var Cylon = require("cylon");
module.exports = function(app) {
// Initialize the robot
Cylon.robot({
  // Change the port to the correct port for your Arduino.
  connections: {
    //arduino: { adaptor: 'firmata', port: '/dev/ttyACM0' }
    arduino: { adaptor: 'firmata', port: 'COM3' }
  },

  devices: {
    led: { driver: 'led', pin: 13 }
  },

  work: function(my) {
    app.route('/api/led/:status').get(function(req, res, next) {
      var status = req.params.status;

      if(status=="on"){
        my.led.turnOn();
      }
      else if(status=="off"){
        my.led.turnOff();
      }
      res.send('Estado led: '+status);
    });
  }
}).start();
}
